// console.log("Hello World!");

//[SECTION] Exponent Operator
//An exponent operator is added to simplify the calculation for the exponent of a given number.
const firstNum = Math.pow(8,2);
console.log(firstNum);

//ES6 update
const secondNum = 8 ** 2;
console.log(secondNum);

//[SECTION] Template Literals
// Allows developer to write strings withouit using the concatenation operator (+)

let name = "John";
// Hello John! Welcome to programming!

// let message = "Hello " + name + "! Welcome to programming!"

// console.log("Message without template literals: \n" + message)

// Uses backticks (``)
// variables are place insied a placeholder(${})
let message = `Hello ${name}! Welcome to programming!`;

console.log(`Message with template literals: ${message}`);

// Multi-line using Template Literals
const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
`
console.log(anotherMessage)

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

// [SECTION] Array Destructuring
// Allows us to unpack elements in arrays into distinct variables.
// Allows developer to name array elements with variables instead of using index number.

const fullName = ["Juan", "Tolits", "Dela Cruz"];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// Hello Juan Tolits Dela Cruz! It's good to see you again.
console.log("Hello " + fullName[0] + " " + fullName[1] + " " + fullName[2] + "! It's good to see you again.");

// Using Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

//using template literals and array destructuring
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you again.`)

// [SECTION] Object Destructuring
//Allows to unpack properties of objects into distinct variables.
//Shortens the syntax for accessiong properties from objects.
//property name should be used in destructuring.

const person = {
	givenName: "Jane",
	maidenName: "Miles",
	familyName: "Doe"
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Hello Jane Miles Doe! It's good to see you again.
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
// this should be exact property name of the object.
const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

const getFullName =({givenName, maidenName, familyName}) => {
	console.log(`This is printed inside a function:`)
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);

// [SECTION] Arrow Function

/*		SYNTAX
	let/const functionName = () =>{
		statement/s;
	}


*/

const hello = () => {
	console.log(`Hello World!`);
}

hello();

// Arrow function with loops
const students = ["John", "Jane","Judy"];
students.forEach((student) => {
	console.log(`${student} is a student.`)


})

// [SECTION] Implicit Return statement

const add = (x,y) =>  x + y;


let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value

const greet = (name = 'User') => {
	return `Good Evening, ${name}!`
}
console.log(greet());
console.log(greet("John"));

// [SECTION] Class-based Object Blueprint

// Creating a class 
class Car{
	constructor(brand,name,year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
// Pwedeng magreassign kay const if object
const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

const myNewCar = new Car("toyota","Vios",2021);
console.log(myNewCar)
